package CoffeeMachine;

public interface CoffeeMachine {
    enum typeDrink {
        ESPRESSO{
            public void making() {
                typeOperation.BOIL.print();
                typeOperation.ADDCOFFEE.print();
                done();
            }
        },
        KAPUCHINO{
            public void making() {
                typeOperation.BOIL.print();
                typeOperation.ADDCOFFEE.print();
                typeOperation.ADDMILK.print();
                done();
            }
        },
        HOTCHOCOLAD{
            public void making() {
                typeOperation.BOIL.print();
                typeOperation.ADDCHOCOLAD.print();
                done();
            }
        },
        HOTWATER{
            public void making() {
                typeOperation.BOIL.print();
                done();
            }
        };
        public abstract void making();
        public void done(){
            System.out.println("Напиток готов!");
        };
    };

    enum typeOperation {
        BOIL{
            @Override
            public void print() {
                System.out.println("Кипячу воду");
            }
        },
        ADDCOFFEE{
            public void print() {
                System.out.println("Добавляю кофе");
            }
        },
        ADDMILK{
            public void print() {
                System.out.println("Добавляю молоко");
            }
        },
        ADDCHOCOLAD{
            public void print() {
                System.out.println("Добвляю какао");
            }
        };

        public abstract void print();
    };

    void makeDrink(typeDrink drink);
}