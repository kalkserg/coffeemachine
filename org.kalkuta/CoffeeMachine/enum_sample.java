package CoffeeMachine;

public class enum_sample {
    public static void main(String[] args) {
        System.out.println("ESSPRESSO");
        new CoffeeMachineImpl().makeDrink(CoffeeMachine.typeDrink.ESPRESSO);

        System.out.println("HOTCHOCOLAD");
        new CoffeeMachineImpl().makeDrink(CoffeeMachineImpl.typeDrink.HOTCHOCOLAD);

        System.out.println("HOTWATER");
        new CoffeeMachineImpl().makeDrink(CoffeeMachineImpl.typeDrink.HOTWATER);

        System.out.println("KAPUCHINO");
        new CoffeeMachineImpl().makeDrink(CoffeeMachineImpl.typeDrink.KAPUCHINO);
    }
}